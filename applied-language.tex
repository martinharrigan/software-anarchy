  \chapter{Applied language}

  We have now discussed how to produce liberatory software and how to
  form cooperative networks to support production, but we need to
  properly liberate and decentralise the subjects of our products now.
  A liberatory technology is fundamentally a product of its
  \emph{society}; even if it is developed in a dynamic manner, and its
  authors are organised, one cannot seriously suggest that it is an
  improvement on anything if it provides leverage for power imbalances
  and abuse.

  \hyphenation{de-central-ised}

  We will examine the stratification of social relationships established
  on ``decentralised'' only in software networks; including how social
  standing is reflected in a social network (which we may as well call
  our society for the sake of argument), and the ways in which a
  mediating moderator can make interactions more difficult than they
  could be. We will then examine the effects of the simplification of
  the means of presentation and how we can communicate on social
  networks, and the effects of making protocol extension part of a
  protocol itself.

  \section{Digital feudalism and social capitalism}

  \begin{quotation}
    \begin{samepage}
      \noindent
      \textsf{<Aurora\_v\_kosmose>} Making any and all storage dependent
      on having dcoin also has the issue of locking out anyone with
      limited monetary means.  If only decentralized systems were easy. \\
      \textsf{<no-defun-allowed>} Indeed, it just perpetuates hierarchy. \\
      \textsf{<aeth>} I mean, that's basically the point of Urbit in
      particular... I'm surprised you didn't mention (or notice?)  the
      neo-feudalism of it \\
      \textsf{<no-defun-allowed>} I did notice it, and I didn't say
      anything because I expected it. \\
      \textsf{<aeth>} heh
      
      \rightline{A conversation in \texttt{\#lispcafe} about cryptocurrencies}
    \end{samepage}
  \end{quotation}

  Many software projects, including free-and-open-source projects and
  even soon-to-be ``ethically licensed'' projects, create hierarchies
  that have no need to exist. Two examples of types of project
  appear to continuously perpetuate hierarchies: cryptocurrencies, for
  self-evident reasons;\footnote{Although there are some currencies that
    attempt to disrupt the implicit capitalism in them, such as Faircoin,
    by using a more direct consensus system instead of proof-of-work or
    proof-of-stake.} and discussion sites of various forms, including
  micro-blogging sites (including Mastodon, Twitter, and so on), and
  online forums (Lemmy, Lobsters, Raddle/Postmill, Reddit, and so on).

  Terms like ``digital feudalism'' or ``social capitalism'' are often
  used to describe hierarchies in socialisation. While some will object
  to misusing terminology like that, we will continue to use it as such,
  as our readers will immediately acknowledge that these are probably
  not good systems to keep around, and that they are good analogies of
  the power structures they describe. We will provide our
  interpretations of these words in the context of \emph{the
    Fediverse}, a network of interconnected micro-blogging servers that
  typically run some software that supports the \emph{ActivityPub}
  messaging protocol, often using \emph{Mastodon} or \emph{Pleroma}
  -- but our commentary is not limited to any specific software and
  protocol used. It is all to do with how this software handles
  moderation, and how people on this user handle interacting with each
  other. Perhaps the more radical projects which end up recreating
  these forms of hierarchy are the most insidious, as one expects them
  to have made an improvement.

  \subsection{Digital feudalism}

  The first system we identified was one of digital feudalism; in
  which moderators, who usually would act as if they are serving their
  serfs, have total and essentially unaccountable control over them. On
  the Fediverse, this analogy is pushed even further than usual, as
  servers and their hosts act as the lords of this system; as it is
  quite difficult to transfer an account between servers, and that
  notion of ``transferring'' loses identity anyway.

  Though we have not found any proponents of feudalism fortunately,
  attempts at suggesting this hierarchy promotes ``autonomy'' sound
  as ridiculous as proponents of ``libertarian'' capitalism (some
  union of propertarians, American libertarians,
  ``anarcho''-capitalists, and so on).

  As hinted at before, excusal of hierarchies when software is
  involved is not exclusive to capitalist apologists! No followers of
  an ideology or lack thereof are particularly more aware of the kind of
  power dynamics they partake in than others, despite any claims
  otherwise; our fellow radicals appear to forget what they learned about
  \emph{the People's Stick} and all those other analogies and critiques
  for power, when they believe they can do self-contradictory things
  like forming ``an organisational model and governance that puts
  marginalised voices first''\footnote{Clipped from
    \url{https://parast.at/}} and provide autonomy for their users by
  strengthening moderation and centralising power into one in-group. In
  the case that a server operator must step in, they must have a
  damned good reason, their actions should almost always be reversible,
  and if not, they better be able to be held accountable if they screw
  up. This na\"ive trust in ``moderators doing moderation'' lead one of
  our colleagues to write a corollary to Bakunin:

  \begin{quotation}
     When the people are being beaten with a stick, they are not much
     happier if it is a particuarly efficient stick, that allows many
     people to be beaten at once.

     \rightline{\cite{dlorah2020}}
  \end{quotation}


  The efficient stick is the norm for Fediverse moderation, as
  improving the efficiency of moderation is generally considered a good
  idea, when moderators can only be trusted to do the right thing; a
  relationship that may or may not be acceptable in the long term (our
  experience leads us to think it is not usually acceptable though).

  But unlike lords of feudal society, the powers associated with
  moderation have significant toil attached to them! While most cases a
  moderator may have to scan through are benign, some cases can require
  time to research what has happened, if it has happened before, in
  order to reach what they believe is the fairest solution, and some
  other cases are so blatantly horrific that moderators looking at them
  are severely disturbed.

  We have heard about both kinds of cases from our friends, but as we
  trend towards larger servers with fewer moderators per user,
  moderation can even become traumatic: a 2018 lawsuit concluded
  recently with Facebook paying out \$52 million to moderators whom
  ``suffered psychological trauma and symptoms of post-traumatic stress
  disorder from repeatedly reviewing violent images''
  \parencite{wong2020}. While we are some magnitudes of growth away from,
  say, having to filter through violent and gruesome imagery frequently,
  a solution appear that would relieve moderators of most of their
  current activities may be well received, and we could agree that they
  would distribute their powers to the community in return; with no doubt
  that the distribution of stress from some of the community would
  greatly improve their mood, resulting in fewer incidents to moderate!
  \emph{Collaborative filtering}, succinctly describable as copying
  actions taken by users that take actions similar to a user, would be
  an ideal mechanism for distributed moderation; it has been used on
  \emph{Usenet} to recommend good articles \parencite{syst1997}, and
  moderation is basically the reverse of that. Using a distributed
  technique ``should be more than enough to effectively automate most
  of the role of [a moderator]'' \parencite{dlorah2020}.

  In short, it is very hard to say who wins in digital feudalism; many
  users are at the whim of tyrannical moderators, or cannot contribute
  without any good moderators, and the magnitude and quality of the
  work moderators must do cannot be healthy for them in the long term.
  Providing users with the ability to collaborate and filter their own
  environment, and reducing the stress of moderators would universally
  improve the subjective quality of the Fediverse.

  \subsection{Social capitalism}

  Even outside the realm of moderation, there is also an imposition of
  some kind of ``ownership'' of discourse, and huge variations in
  social standing, which appear to directly affect how one
  communicates with others. We call this \emph{social capitalism},
  because just like actual capitalism, one with enough capital can do
  almost anything, regardless of harm caused, and escape punishment
  just by having \emph{social capital}. The words uttered when one
  gets on another's nerves are no longer ``fuck off'' or ``get lost'',
  they now try to show force by indicating ownership of the
  conversation! ``Get off my timeline!'' ``Fuck outta of my
  mentions!''\footnote{Mentions do force one to read something they
    don't want to, though; but it is possible to mute conversations
    and not hear more of it.}  Of course, one does not need to justify
  attempting to rid themselves of unpleasant people; but the same
  phrases are also frequently used to avoid being held accountable for
  providing misinformation, or being unpleasant themselves.

  The belief that a conversation somehow belongs to someone, and that
  they have some authority over it even, is highly erroneous.  If we
  were to assume that someone could own a conversation, we may as well
  analyse a conversation as if it were a commodity. It is clear that
  any notion of value is produced by whoever continues and reads the
  conversation. The role of a host of any shape is greatly overstated;
  Kleiner notes that ``the real value of [sites that share
  community-created value are] not created by the developers of the
  site; rather, it is created by the people who upload [content] to
  the site.'' \parencite{kleiner2010} A conversation in a public space
  derives value from all participants in the conversation, and so any
  one of the participants has an equal claim to ``owning'' it.
  
  Of course, the road to hell is paved with good intentions, including
  a suggestion we recently read that ``[we need] posts that can only
  be replied to by mutuals but public and shareable freely''. While
  this admittedly sounds very nice, it is trivial to abuse, and is a
  clear continuation of this myth of social property. The assumption
  here extends to expecting that the person who posted the first
  message is going to be polite. When that person is being impolite,
  restricting replies but freely allowing sharing is quite the
  opposite of what may be desirable.
  
  Some accusations of bad behaviour on social media only make sense
  when we assume starting a conversation provides control. For
  example, the concept of a \emph{reply guy} is meaningless. Suppose a
  user named Alice publishes something, and a social media platform
  decides it is somehow interesting to another user Bob, because Bob
  follows another user Charlie, and Charlie indicated interest in
  Alice's publication (somehow). However, Bob does not think the
  published material is any good, and replies with some
  correction. Alice, in turn, does not like Bob's response.

  But who is the bad user here? Bob saw something he didn't like
  because of a false assumption made by the social media platform,
  that he is always interested in what Charlie is interested in.
  Alice saw something she didn't like because of a false assumption
  made by the social media platform, that she is interested in what
  anyone who found her publication has to say. We can only imagine
  that people who belive that there is a difference in these
  situations, and thus a judgement to be made, just don't find
  themselves in disagreement too often. Much like how critics of
  digital privacy measures claim they have ``nothing to hide'', it
  might be likely that one only stops making their ethical judgements,
  which are only based on assumed rights to power, when they are no
  longer convinced that the assumption benefits them somehow.

  As such, indicating that this would produce a \emph{safe space} is
  also misleading; as with politeness outside the Internet, one who
  publishes publicly should expect to treat their readers as they
  would like to be treated themself, and there are no such guarantees
  in an asymmetrical environment. If we had to make a comparison to a
  discussion in a physical crowd, one could as well be shouting
  disinformation into a crowd on the street, and expecting no one to
  shout back.

  These examples are quite nasty, and we may sound like we can only
  imagine the worst by highlighting only those. Humourous comments
  such as ``Reply to win \emph{a valuable thing}'', and
  ``\emph{Blatantly wrong statement}: change my view'' where the
  reader is unable to reply, are less grim examples of the same
  issues, which don't cause any harm to any other users. The issues
  are only used for comedic effect, as the comments are deliberately
  written to try to get a response from the reader, but they are
  unable to do so, despite that it would be beneficial for them.

  Both social capitalism and digital feudalism are forms of
  \emph{asymmetricality} in power, which is anathematic to
  socialisation and establishing any kind of relationship or mutual
  trust, allowing one to write whatever and repress anyone seeking to
  hold them accountable. It is no less important to abolish
  social asymmetry than it is to abolish asymmetry in
  computer systems, and a decentralised computer system cannot
  try to enforce the former with the latter. While the demands ten years
  ago may have been to establish decentralised networks, we now demand
  social decentralisation of the resulting networks which only exhibit
  decentralisation in computer networks.

  \section{The means of presentation and extension}

  The inverse of a ``radical'' proprietary software-producing firm as
  imagined in \emph{Source materials} has appeared many times before,
  with ``free software'' projects that are run like proprietary
  projects, such as the \emph{Signal} messaging program. The Signal
  developers have many arbitrary and unenforceable constraints on their
  users, like disallowing users from using their own modified
  clients\footnote{See this rule in action in
    \url{https://github.com/LibreSignal/LibreSignal/issues/37} and a blog
    post about how it must feel to be one of those puny decentralised
    protocols that can't do anything at
    \url{https://signal.org/blog/the-ecosystem-is-moving/}. We strongly
    advise against reading either sober.} because it somehow slows
  down making changes. Long ago, we wrote about how this is the opposite
  of what has actually happened in the Common Lisp community, how
  network effects would shift a userbase to unanimously using or not
  using a feature, and how some abstract thinking can relieve
  implementers of having to deal with updates:

  \begin{quotation}
    Other standards like \emph{Bordeaux threads} and \emph{Gray
      streams} have seen widespread implementation [in implementations
    of Common Lisp], despite not being in the ANSI Common Lisp
    standard, or other authoritative documentation. De-facto standards
    such as these are very popular simply because everyone can use
    them, creating a kind of network effect in a userbase.

    [Moxie] attributes his issues to ``XEPs that aren't consistently
    applied anywhere'', but if someone wants to run a server or entry
    point into a federated network, they should be up-to-date on new
    extensions; not doing so repels users. Thanks to the network
    effect previously mentioned, users of inferior servers can move to
    better ones, probably the ones that their connections use.

    With a layer of indirection, you can just push out ``code'' (well,
    it can be Turing complete if you want) to add new features and types
    to a program.

    \rightline{\cite{patton2018}}
  \end{quotation}

  It can easily be said that this was a poor attempt at arguing
  whatever point was being made then; but when ``adding features'' is
  part of one's protocol, then one only has to implement that protocol
  once. This line of thinking lead to the
  \emph{replicated object system} design of \emph{Netfarm}, one of
  our experiments in producing what we would consider anarchist
  software, and the implications of such a design may be very
  interesting.

  The \emph{means of presentation} are the forms in which a
  communication platform allows one to express or present an idea. For
  example, a microblogging site allows expression in text with fewer
  than some number of characters (often allowing uploading other files,
  displaying images inline), a technical journal allows expression in
  articles, a forum allows expression primarily in posts and comments,
  and so on.

  Diverse information demands diverse representations, and forcing
  information across many services that each handle a specific means of
  presentation and representation creates fragmentation and hampers
  discoverability.

  \begin{quotation}
    The first one is the Marxist notion of a general intellect. With
    today’s platforms, we are not facing such a phenomenon. Our use of
    contemporary digital platforms is extremely fragmented and there is no
    such thing as progress of the collective intelligence of the entire
    working class or society. Citizens are facing relentless efforts
    deployed by digital capitalists to fragment, standardise, and
    `taskify' their activities and their very existences.

    \rightline{\cite{casilli2018}}
  \end{quotation}

  Opening up a platform to accept any means of presentation would
  annihilate any gimmicks or distinguishing features of it, but that may
  be the most interesting approach possible, as such a platform
  could present any information in the most appropriate way. Even with
  the media that common platforms support, support is limited to a
  lousy subset, usually prohibiting typesetting of mathematical
  equations, referencing, and sometimes even basic formatting.\footnote{
    It may be argued that opening up the means of presentation may make
    it inaccessible, as some formats are difficult to interpret by some
    users. However, alternative presentations can be recovered at
    the least, which is not the case for workarounds for less expressive
    means of presentation.} While, say, Mastodon and
  {\fontfamily{lm}\selectfont\LaTeX} both transmit text in some form,
  the former is evidently more suitable for near-real-time communication,
  whereas the latter is more suitable for long-form writings, such as
  this book. It would not be hard to give the former the capabilities of
  the latter: some servers already provide mathematical typesetting and
  formatting options.

  Beyond that, there are many more advanced means of presentation which
  can be immediately seen to have uses, that are not even close to
  implementable on the common platforms of today, such as viewing
  three-dimensional objects, dynamic and randomised mediums like
  soundscapes, and simulations of natural phenomena (such as the
  demonstration of approximating a \emph{Bates distribution} in
  Figure~\ref{fig:simulation}). Should our dreams of casual programming
  (as mentioned in Chapter 1) come true, it would not be hard to believe
  sharing programs directly would be commonplace.

  \begin{figure}[t]
    \centering
    \includegraphics[width=0.95\textwidth]{explaining-with-a-simulation}
    \caption{Providing a simulation that the reader can run can be much
      more illustrative than using a static medium to make a point.}
    \label{fig:simulation}
  \end{figure}

  However, it should not be necessary to modify a server to provide
  these new means of presentation. To modify the behaviour of the
  platform without modifying the platform itself, the platform will have
  to communicate in programs and/or objects that describe and present
  themselves, instead of text and/or plain media formats. Implementing
  such a platform may be very difficult to begin with, but it is much
  more tedious to incrementally extend a platform, such as the Web or
  some protocol residing on it such as ActivityPub, that merely display
  documents and texts.  For reference, the \emph{Chromium} web browser
  contains about 34 million lines of code, and a complete
  \emph{Squeak} Smalltalk environment contains only about 5 million; 4
  million lines in \emph{OpenSmalltalk}, a just-in-time compiling
  virtual machine, and 1 million lines in the Smalltalk environment,
  including a graphical environment, bytecode compiler, debugger and
  class browser, and some other components that do not appear in a
  browser, including an email client, graphics framework, and package
  manager.\footnote{There are quite a few debugging tools in a browser;
    many provide debuggers, profilers and whatnot, but they do not usually
    contain complete programming environments.}

  Such a total lines-of-code count could be misleading as to how
  much work an implementer has to do; much of the complexity would be
  reusable with almost no modification per implementation, as it would
  make up the distributed ``image'' the client retrieves.
  Furthermore, a large subset of the functionality of modern Squeak
  could likely be significantly smaller; the first implementation of the
  Squeak virtual machine was written in only about eight thousand lines
  of code \parencite{ingalls1997}.

  This is still magnitudes more code than the more common reaction to
  the complexity of the Web would require, in which protocols are
  proposed that are intended to only display ``documents''. As we have
  mentioned, what constitutes presenting a text document is very dubious,
  yet most suggestions provide very little to work with when tasked with
  book-making. \emph{Gemini} is one example of this reaction,
  accompanied with some vague, nostalgic association with the ``essence of
  the Web''. It is supposed you can write a Gemini client in less than a
  few hundred lines of code,\footnote{``Experiments suggest that a very
    basic interactive client takes more like a minimum of 100 lines of
    code, and a comfortable fit and moderate feature completeness need
    more like 200 lines.''
    \url{https://gemini.circumlunar.space/docs/faq.html}} yet the end
  result is sending uninteractive text with minimal formatting
  across a network. For what it can produce, the result is hardly an
  advancement over the printing press! We don't need a computer to publish
  text documents; pen and paper, and either patience or a photocopier
  would suffice. And, of course, you can draw and format the text however
  you like with pen and paper. What we need instead is a better book:

  \begin{quotation}
    We do not feel that technology is a \emph{necessary} constituent
    for this process any more than is the book. It may, however, provide
    us with a better ``book'', one which is active \textelp{}
    rather than passive. It may be something with the attention grabbing
    powers of TV, but controllable by [the user] rather than the networks.
    It can be like a piano: a product of technology, yes, but one which
    can be a tool, a toy, a medium of expression, a source of unending
    pleasure and delight\ldots{}and as with most gadgets in unenlightened
    hands, a terrible drudge!

    \rightline{\cite{kay1972}}
  \end{quotation}

  A computer is a ``universal simulator'', and so with the appropriate
  peripheral devices, it is a form of \emph{meta-media}, in which its
  users construct other forms of media, such as books, animations,
  programs, and so on. (Some of these forms of media even could only
  exist on a computer, such as video games.) Reducing the forms of
  media one can produce is antithetical to the concept of computing
  itself, and makes any labour put into designing computers and their
  media look pointless. For example, an article published by
  Crimethinc\footnote{\url{https://crimethinc.com/2003/12/01/destructive-production-can-we-really-expect-to-manufacture-complex-technologies-in-an-anarchist-society}},
  which detailed the current extraction and hazards of manufacturing
  computers quite well, conveniently did not mention the ecological
  impacts of manufacturing the media that a computer could
  replace. The imagery of poisonous fumes, factory lines, and so on,
  is never contrasted with the equivalent dangers of the processes of
  manufacturing books, tapes, mechanical devices, and the other analog
  medias; let alone even leaving a mechanical, monotonous process to a
  few people, whom work shifts keeping a boring system regulated,
  instead of having a computer do it.  It is not so unrealistic to
  compare the creation of \emph{one} computer to \emph{hundreds} of
  books and tapes, and the lifespans wasted by people performing the
  dull work themselves simultaneously. But if we continue with the
  simplification-at-all-costs which the likes of the Gemini protocol
  promise, such a comparison would provide much better evidence in
  favour of avoiding computing.

  With some consideration of protocol self-extension, we can happily
  intermingle many different modes of expression and representation.
  With the normalisation of protocol self-extension, we could greatly
  reduce the toil protocol implementers must perform, while not
  compromising on the expression of the protocol. Messing with
  a protocol may become an emerging mode of expression in itself, like
  Dadaists had done for poetry and art, and demoscene programmers do
  with the obscure features (optionally to be read in air-quotes) of
  computer hardware. In any case, an appropriate meta-means of
  presentation can vastly expand the information that can be
  communicated on one protocol and one set of implementations.
